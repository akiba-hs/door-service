
all: build

build: generate

generate: generate-api
	go generate

generate-api: api/v1/swagger.yml
	oapi-codegen -generate fiber -package v1 api/v1/swagger.yml > api/v1/fiber.go
	oapi-codegen -generate types -package v1 api/v1/swagger.yml > api/v1/types.go
